/**
*BALDWIN CEPEDA CSC217 
*N00894593
*LAB 1 
*BALDWINCEPEDA@GMAIL.COM
*/
#include <stdbool.h>
#ifndef _main_h_
#define _main_h_

bool myStrcmp(char s[], char t[]);  
int krgetline(char s[], int lim);
int myStringLen(char s[]);
int myIndexOf(char s[], char c);


#endif
/**
 *BALDWIN CEPEDA CSC217 
 *N00894593
 *LAB 1 
 *BALDWINCEPEDA@GMAIL.COM
 */
#include <stdio.h>

#include <string.h>

#include "header.h"

#include <stdbool.h>

/*
 *String compare written in class returns true if they are the same
 */
bool myStrcmp(char s[], char d[]) {
    int i = 0;
    for (; d[i] == s[i]; i++)
        if (!s[i])
            return true;

    return false;
}
/**
 *My string length function written in class
 */
int myStringLen(char s[]) {
    int i = 0;
    for (; s[i]; i++)
    ;
    return i;
}


/**
 *reads input from the user: written in class 
 */
int krgetline(char s[], int lim) {
    char c;
    int i;
    for (i = 0; i < lim - 1 && (c = getchar()) != '\n'; ++i) {
        s[i] = c;

        if (c == '\n') {
            s[i] = c;
            ++i;
        }
    }
    s[i] = '\0';
    return i;

}
/**
 *My index of function written in class
 */
int myIndexOf(char s[], char c) {
    int i = 0;
    for (; s[i]; i++)
        if (s[i] == c)
            return i;

    return -1; // this mean our target was never found 
}